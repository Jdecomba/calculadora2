package com.jdecomba.solver;

import com.jdecomba.operation.Operations;
import java.util.List;

public class Solver {
    
    /**
     * Solves a given equation.If the list <tt>operationInputs</tt>  
 has more than 1 operation, it solves it.Otherwise, it returns the first given input from the list <tt>allInputs</tt>.  
     *
     * @param operationInputs List of all the operations for the equation 
     * @param allInputs List of all the inputs for the equation
     * @param allOperations List of all the available operations
     * @param operations Class that have all the operations
     * @return a <tt>String</tt> from the result of the equation.
     */
    public String solveEquation(List<String> operationInputs, List<String> allInputs, List<String> allOperations, Operations operations){
        Boolean goodSyntax = true;
        String result = "";
        
        //While there's some operation
        while(!operationInputs.isEmpty() || allInputs.size()!=1){ 
            int posOperator;
            int posInput;
            Double n1;
            String currOperator;                            
            //If the operation is log
            if(operationInputs.contains("log")){ 
                posOperator = operationInputs.indexOf("log");
                posInput = allInputs.indexOf("log");

                currOperator = operationInputs.get(posOperator);                                 

                StringBuilder bld = new StringBuilder();

                for (int i = posInput+1; i < allInputs.size(); i++) {
                    if(allOperations.contains(allInputs.get(i))){
                        break;
                    }else{
                        bld.append(allInputs.get(i));
                    }
                }
                String number = bld.toString();
                n1 = parseNumber(number);

                System.out.println("Operation: " + currOperator + n1);                                 
                operationInputs.remove(posOperator);                                                                                               
                allInputs.set(posInput,String.valueOf(operations.logarithm(n1)));                                                                                                 

                for (int i = 0; i < number.length(); i++) {
                    allInputs.remove(posInput+1);
                }                                                                
            }
            //If the operation is root or power   
            else if(operationInputs.contains("√") || operationInputs.contains("^")){                                                                        
                goodSyntax = solveOperation("√","^",operationInputs,allInputs,allOperations,operations);                                        
            }
            //If the operation is percent
            else if(operationInputs.contains("%")){
                goodSyntax = solveOperation("%","?",operationInputs,allInputs,allOperations,operations);                                                                            
            }
            //If the operation is multiply or divide
            else if(operationInputs.contains("*") || operationInputs.contains("/")){
                goodSyntax = solveOperation("*","/",operationInputs,allInputs,allOperations,operations);                                        
            }
            //If the operation is add or subtract
            else if(operationInputs.contains("+") || operationInputs.contains("-")){
                goodSyntax = solveOperation("+","-",operationInputs,allInputs,allOperations,operations);                                                                                                                                                                                                                                                                      
            }
            //If something went wrong with the operation, set the result as 'Math Error'   
            if(!goodSyntax){
                result = "Math Error";
                break;
            }            
        }
        //If all went good with the operation, get the result from allInputs
        if(goodSyntax){
            result = allInputs.get(0);
        }
        System.out.println("Solved Equation: "+result);           
        return result;
    }
    /**
     * Solves a given operation.Recieves 2 operations <tt>op1</tt> and <tt>op1</tt>            
 and checks the first aparition from both of them.Then it solves the leftmost operation.
     *
     * @param op1 First operation
     * @param op2 Second operation 
     * @param operationsInputs List of all the operations entered 
     * @param allInputs List of all the inputs for the operation 
     * @param allOperations List of all the available operations 
     * @param operations Class that have all the operations 
     * @return a <tt>Boolean</tt> indicating if the result of the operation was successful.
     */    
    public Boolean solveOperation(String op1, String op2, List<String> operationsInputs, List<String> allInputs, List<String> allOperations, Operations operations){
        int posOperator;
        int posInput;
        int firstOpPos;
        int secondOpPos;
        int count1 = 0;
        int count2 = 0;
        Double n1;
        Double n2;
        String operator;
        String number1 = "";
        String number2 = "";
        
        firstOpPos = operationsInputs.indexOf(op1);
        secondOpPos = operationsInputs.indexOf(op2);
        
        //Get the leftmost operator position from operationsInputs
        if((firstOpPos != -1) && (secondOpPos != -1)){
            posOperator = Math.min(firstOpPos,secondOpPos);                                                                                                                                                                                
        }else if(firstOpPos == -1){
            posOperator = secondOpPos;                                    
        }else{
            posOperator = firstOpPos;
        }
        
        //Get the leftmost operator
        operator = operationsInputs.get(posOperator);
        //Get the leftmost operator index from allInputs
        posInput = allInputs.indexOf(operator);                                        
        
        StringBuilder bld1 = new StringBuilder();
        StringBuilder bld2 = new StringBuilder();
        
        //Get the first number
        for (int i = posInput-1; i > -1; i--) {
            if(allOperations.contains(allInputs.get(i))){
                break;
            }else{
                count1++;
                bld1.append(allInputs.get(i));
            }
        }
        number1 = bld1.toString();
        //Get the second number
        for (int i = posInput+1; i < allInputs.size(); i++) {
            if(allOperations.contains(allInputs.get(i))){
                break;
            }else{
                count2++;
                bld2.append(allInputs.get(i));
            }
        }
        number2 = bld2.toString();
        //Parses the numbers
        if(count1==1){                                    
            n1 = parseNumber(number1);                                  
        }else{
            StringBuilder sb = new StringBuilder(number1);                                 
            n1 = Double.parseDouble(sb.reverse().toString());                                                                                                 
        }                               
        n2 = parseNumber(number2);  
                                
        System.out.println("Operation: "+ n1 + operator + n2);
        //Remove the operation from the list of operations
        operationsInputs.remove(posOperator);
        
        //Get the result according with the operation
        switch (operator) {
            case "+":
                allInputs.set(posInput,String.valueOf(operations.add(n1,n2))); 
                break;
            case "-":
                allInputs.set(posInput,String.valueOf(operations.subtract(n1,n2)));
                break;
            case "/":
                if(n2==0){
                    return false;
                }else{
                    allInputs.set(posInput,String.valueOf(operations.divide(n1,n2)));
                }                                
                break;
            case "*":
                allInputs.set(posInput,String.valueOf(operations.multiply(n1,n2)));
                break;
            case "^":
                allInputs.set(posInput,String.valueOf(operations.power(n1,n2)));
                break; 
            case "√":
                allInputs.set(posInput,String.valueOf(operations.square(n1,n2)));
                break;                                
            case "%":
                allInputs.set(posInput,String.valueOf(operations.percentage(n1,n2)));
                break;                
            default:
                break;
            }        
                
        //Remove second number
        for (int i = 0; i < count2; i++) {
            allInputs.remove(posInput+1);
        }                                                                

        //Remove first number
        for (int i = 1; i <= count1; i++) {
            allInputs.remove(posInput-i);
        }                                                                                                                                                                                                
        
        return true;
    }
    /**
     * Parses a given String to Double.Recieves a String <tt>n</tt> and according
 with its content, returns the value.
     * 
     * @param n The number to parse
     * @return a <tt>Double</tt> with the according value.
     */    
    public Double parseNumber(String n){
        Double number;
        //If the number is PI
        switch (n) {
            case "π":
                number=Math.PI;
                break;
            case "e":
                number=Math.E;
                break;
            default:
                number = Double.parseDouble(n);
                break;
        }
        return number;
    }        
}