package com.jdecomba.gui;

import com.jdecomba.solver.Solver;
import com.jdecomba.operation.Operations;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
 
public class CalculadoraGui extends JFrame { 
    int numLeftParentheses = 0;
    int numRightParentheses = 0;
    int currentResult = -1;
    int extraWindowWidth = 100;
    String operator = "";
    String titleTab1 = "Operaciones";
    String titleTab2 = "Conversiones";
    String[] typesOfConversions = {"Volumen","Longitud","Temperatura","Peso","Presión"};          
    String[] volumeUnits = {"cm³","m³","L","Gal"};   
    String[] longitudeUnits = {"inch","mm","cm","m","km"};   
    String[] temperatureUnits = {"C°","F°","K"};   
    String[] weightUnits = {"gr","kg","ton","lb"};   
    String[] pressureUnits = {"Pa","atm","mmHg"};   
    Double[] volumeEquivalence = {1000000.0,1.0,1000.0,264.172};   
    Double[] longitudeEquivalence = {39.3701,1000.0,100.0,1.0,0.001};   
    Double[] weightEquivalence = {1000.0,1.0,0.001,2.20462};   
    Double[] pressureEquivalence = {101325.0,1.0,760.0};       
    Color operationBtnColor = Color.ORANGE;
    Color funcBtnColor = new java.awt.Color(60,179,113);
    Color calcBtnColor = Color.RED;            
    JTextArea inputAreaOperations = new JTextArea(3, 18);
    JTextArea inputAreaConversions = new JTextArea(3, 18);
    CustomButton zero = new CustomButton("0",1,inputAreaOperations);
    CustomButton one = new CustomButton("1",1,inputAreaOperations);
    CustomButton two = new CustomButton("2",1,inputAreaOperations);
    CustomButton three = new CustomButton("3",1,inputAreaOperations);
    CustomButton four = new CustomButton("4",1,inputAreaOperations);
    CustomButton five = new CustomButton("5",1,inputAreaOperations);
    CustomButton six = new CustomButton("6",1,inputAreaOperations);
    CustomButton seven = new CustomButton("7",1,inputAreaOperations);
    CustomButton eight = new CustomButton("8",1,inputAreaOperations);
    CustomButton nine = new CustomButton("9",1,inputAreaOperations);
    CustomButton pi = new CustomButton("π",1,inputAreaOperations);
    CustomButton euler = new CustomButton("e",1,inputAreaOperations);   
    CustomButton point = new CustomButton(".",1,inputAreaOperations);
    CustomButton plus = new CustomButton("+",2,inputAreaOperations);
    CustomButton minus = new CustomButton("-",2,inputAreaOperations);
    CustomButton times = new CustomButton("*",2,inputAreaOperations);
    CustomButton divide = new CustomButton("/",2,inputAreaOperations);
    CustomButton log = new CustomButton("log",2,inputAreaOperations);
    CustomButton percent = new CustomButton("%",2,inputAreaOperations);
    CustomButton pow = new CustomButton("^",2,inputAreaOperations);
    CustomButton squaren = new CustomButton("y√x",2,inputAreaOperations);    
    CustomButton pow2 = new CustomButton("^2",2,inputAreaOperations);
    CustomButton sqrt = new CustomButton("2√",2,inputAreaOperations);    
    CustomButton equals = new CustomButton("=",3,inputAreaOperations);
    CustomButton clear = new CustomButton("CLR",3,inputAreaOperations);
    CustomButton ans = new CustomButton("ANS",3,inputAreaOperations);
    CustomButton parentheses1 = new CustomButton("(",3,inputAreaOperations); 
    CustomButton parentheses2 = new CustomButton(")",3,inputAreaOperations);     
    CustomButton prevAns = new CustomButton("/\\",3,inputAreaOperations);
    CustomButton nextAns = new CustomButton("\\/",3,inputAreaOperations);
    CustomButton off = new CustomButton("OFF",4,inputAreaOperations);
    CustomButton convertButton = new CustomButton("Convert",3,inputAreaConversions);    
    CustomButton clearConvert = new CustomButton("CLR",3,inputAreaConversions);
    JTabbedPane tabsPane = new JTabbedPane(JTabbedPane.BOTTOM);
    JPanel operationsBtnPanel = new JPanel();
    JPanel conversionsBtnPanel = new CustomNumberPanel(inputAreaConversions);
    JPanel tabOperations = new JPanel();
    JPanel tabConversions = new JPanel();    
    JList listOfConversions = new JList();
    JList unitOfMeasure1 = new JList();
    JList unitOfMeasure2 = new JList();
    JTextField resultAreaOperations = new JTextField(15);
    JTextField resultAreaConversions = new JTextField(15);
    List<String> allOperations = new ArrayList<>();
    List<String> operationInputs = new ArrayList<>();
    List<String> allInputs = new ArrayList<>();    
    List<String> allResults = new ArrayList<>();    
    Operations operations = new Operations();
    Solver solver = new Solver();
        
    class CustomListener implements ActionListener{
        private final String txt;
        private final int type;        
        private final JTextArea inputArea;  
        
        public CustomListener(String txt, int type, JTextArea inputArea){
            this.txt=txt;
            this.type=type;    
            this.inputArea = inputArea;
        }
        @Override
        public void actionPerformed(ActionEvent e) {                        
            switch (type) {
                case 1:
                    this.inputArea.append(txt);
                    allInputs.add(txt);
                    break;
                case 2:
                    this.inputArea.append(txt);
                    allInputs.add(txt);
                    operationInputs.add(txt);
                    break;                    
                default:                    
                    break;
            }                                
        }
    }
    
    class CustomButton extends JButton{ 
        private CustomButton(String txt,int type,JTextArea inpArea){
            super.setText(txt);
            
            switch (type) {
                case 1:
                    break;
                case 2:
                    super.setBackground(operationBtnColor);
                    break;
                case 3:
                    super.setBackground(funcBtnColor);
                    break;
                case 4:
                    super.setBackground(calcBtnColor);
                    break;                                        
                default:                    
                    break;
            }
            
            super.addActionListener(new CustomListener(txt,type,inpArea));            
        }
    }
    
    class CustomNumberPanel extends JPanel{
        private CustomNumberPanel(JTextArea inpArea){
            CustomButton zero = new CustomButton("0",1,inpArea);
            CustomButton one = new CustomButton("1",1,inpArea);
            CustomButton two = new CustomButton("2",1,inpArea);
            CustomButton three = new CustomButton("3",1,inpArea);
            CustomButton four = new CustomButton("4",1,inpArea);
            CustomButton five = new CustomButton("5",1,inpArea);
            CustomButton six = new CustomButton("6",1,inpArea);
            CustomButton seven = new CustomButton("7",1,inpArea);
            CustomButton eight = new CustomButton("8",1,inpArea);
            CustomButton nine = new CustomButton("9",1,inpArea);
            CustomButton point = new CustomButton(".",1,inpArea);

            super.add(seven);        
            super.add(eight);        
            super.add(nine); 
            super.add(four);        
            super.add(five);        
            super.add(six);        
            super.add(one);        
            super.add(two);        
            super.add(three);               
            super.add(point);        
            super.add(zero);                    
            super.add(new JButton());        
        }
    }
        
    public static void main(String[] args) {        
        CalculadoraGui calculatorGUI = new CalculadoraGui();                        
        calculatorGUI.setSize(300, 400);
        calculatorGUI.setTitle("Calculadora");
        calculatorGUI.setResizable(false);
        calculatorGUI.setVisible(true);
        calculatorGUI.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);                        
    }
    
    public CalculadoraGui() {                                                            
        //Init Variables        
        //TAB OPERACIONES
        tabOperations = new JPanel() {
            @Override
            public Dimension getPreferredSize() {
                Dimension size = super.getPreferredSize();
                size.width += extraWindowWidth;
                return size;
            }
        };
                
        //TAB CONVERSIONES
        tabConversions = new JPanel() {
            @Override
            public Dimension getPreferredSize() {
                Dimension size = super.getPreferredSize();
                size.width += extraWindowWidth;
                return size;
            }
        };
        //create list of conversion types                
        listOfConversions = new JList(typesOfConversions); 
        listOfConversions.setSelectedIndex(0); 
        listOfConversions.addListSelectionListener((ListSelectionEvent arg0) -> {
            if (!arg0.getValueIsAdjusting()) {
                switch(listOfConversions.getSelectedIndex()){
                    case 0:
                        unitOfMeasure1.setListData(volumeUnits);
                        unitOfMeasure1.setSelectedIndex(0);
                        unitOfMeasure2.setListData(volumeUnits);
                        unitOfMeasure2.setSelectedIndex(0);
                        break;
                    case 1:
                        unitOfMeasure1.setListData(longitudeUnits);
                        unitOfMeasure1.setSelectedIndex(0);
                        unitOfMeasure2.setListData(longitudeUnits);
                        unitOfMeasure2.setSelectedIndex(0);
                        break;
                    case 2:
                        unitOfMeasure1.setListData(temperatureUnits);
                        unitOfMeasure1.setSelectedIndex(0);
                        unitOfMeasure2.setListData(temperatureUnits);
                        unitOfMeasure2.setSelectedIndex(0);
                        break;
                    case 3:
                        unitOfMeasure1.setListData(weightUnits);
                        unitOfMeasure1.setSelectedIndex(0);
                        unitOfMeasure2.setListData(weightUnits);
                        unitOfMeasure2.setSelectedIndex(0);
                        break;
                    case 4:
                        unitOfMeasure1.setListData(pressureUnits);
                        unitOfMeasure1.setSelectedIndex(0);
                        unitOfMeasure2.setListData(pressureUnits);
                        unitOfMeasure2.setSelectedIndex(0);
                        break;
                    default:
                        break;                                                            
                }
            }
        });
        //UI settings
        inputAreaOperations.setForeground(Color.BLACK);
        inputAreaOperations.setBackground(Color.WHITE);
        inputAreaOperations.setLineWrap(true);
        inputAreaOperations.setWrapStyleWord(true);
        inputAreaOperations.setEditable(false);
        inputAreaOperations.setFont(inputAreaOperations.getFont().deriveFont(15f));
        resultAreaOperations.setEditable(false);
        resultAreaOperations.setFont(resultAreaOperations.getFont().deriveFont(20f));
        resultAreaOperations.setHorizontalAlignment(SwingConstants.RIGHT);        
        resultAreaOperations.setForeground(Color.BLACK);
        resultAreaOperations.setBackground(Color.WHITE);                
        resultAreaOperations.setEditable(false);                

        inputAreaConversions.setForeground(Color.BLACK);
        inputAreaConversions.setBackground(Color.WHITE);
        inputAreaConversions.setLineWrap(true);
        inputAreaConversions.setWrapStyleWord(true);
        inputAreaConversions.setEditable(false);
        inputAreaConversions.setFont(inputAreaConversions.getFont().deriveFont(15f));
        resultAreaConversions.setEditable(false);
        resultAreaConversions.setFont(resultAreaConversions.getFont().deriveFont(20f));
        resultAreaConversions.setHorizontalAlignment(SwingConstants.RIGHT);        
        resultAreaConversions.setForeground(Color.BLACK);
        resultAreaConversions.setBackground(Color.WHITE);                
        resultAreaConversions.setEditable(false);
        
        operationsBtnPanel.setLayout(new GridLayout(8, 0));    
        conversionsBtnPanel.setLayout(new GridLayout(4, 0));    
        //ROW 1 of Buttons        
        operationsBtnPanel.add(clear);        
        operationsBtnPanel.add(parentheses1);
        operationsBtnPanel.add(parentheses2);
        operationsBtnPanel.add(off);        
        //ROW 2 of Buttons
        operationsBtnPanel.add(log);
        operationsBtnPanel.add(percent);
        operationsBtnPanel.add(pow);
        operationsBtnPanel.add(prevAns);        
        //ROW 3 of Buttons
        operationsBtnPanel.add(pow2);        
        operationsBtnPanel.add(squaren);
        operationsBtnPanel.add(sqrt);
        operationsBtnPanel.add(nextAns);      
        //ROW 4 of Buttons                
        operationsBtnPanel.add(pi);        
        operationsBtnPanel.add(euler);
        operationsBtnPanel.add(new JButton("  "));                
        operationsBtnPanel.add(divide);
        //ROW 5 of Buttons
        operationsBtnPanel.add(seven);
        operationsBtnPanel.add(eight);
        operationsBtnPanel.add(nine);
        operationsBtnPanel.add(times);
        //ROW 6 of Buttons
        operationsBtnPanel.add(four);
        operationsBtnPanel.add(five);
        operationsBtnPanel.add(six);
        operationsBtnPanel.add(minus);
        //ROW 7 of Buttons
        operationsBtnPanel.add(one);
        operationsBtnPanel.add(two);
        operationsBtnPanel.add(three);
        operationsBtnPanel.add(plus);        
        //ROW 8 of Buttons
        operationsBtnPanel.add(point);
        operationsBtnPanel.add(zero);               
        operationsBtnPanel.add(ans); 
        operationsBtnPanel.add(equals);       
        //Add Elements of Tab1
        tabOperations.add(new JScrollPane(inputAreaOperations),BorderLayout.NORTH);        
        tabOperations.add(resultAreaOperations);               
        tabOperations.add(operationsBtnPanel,BorderLayout.SOUTH);        
        //Add Elements of Tab2  
        tabConversions.add(new JScrollPane(inputAreaConversions),BorderLayout.NORTH);
        tabConversions.add(resultAreaConversions);         
        JPanel p = new JPanel(); 
        p.add(listOfConversions); 
        tabConversions.add(p);
                
        tabConversions.add(new JLabel("De"));
        
        unitOfMeasure1 = new JList(volumeUnits); 
        unitOfMeasure1.setSelectedIndex(0);        
        unitOfMeasure1.setFont(unitOfMeasure2.getFont().deriveFont(15f));
        tabConversions.add(unitOfMeasure1);
        
        tabConversions.add(new JLabel("a"));
        
        unitOfMeasure2 = new JList(volumeUnits); 
        unitOfMeasure2.setSelectedIndex(0);
        unitOfMeasure2.setFont(unitOfMeasure2.getFont().deriveFont(15f));
        tabConversions.add(unitOfMeasure2);                      
        tabConversions.add(conversionsBtnPanel,BorderLayout.SOUTH);        
        tabConversions.add(convertButton);
        tabConversions.add(clearConvert);        
        //Add tabs to Tabbed Panel
        tabsPane.addTab(titleTab1, tabOperations);
        tabsPane.addTab(titleTab2, tabConversions);                   
        //Add Tabbed Panel to JFrame
        super.add(tabsPane, BorderLayout.CENTER);
        
        initOperations();
        addListeners();
    }

    private void initOperations(){
        allOperations.add("log");
        allOperations.add("√");
        allOperations.add("^");
        allOperations.add("%");
        allOperations.add("*");
        allOperations.add("/");
        allOperations.add("+");
        allOperations.add("-");
        allOperations.add("(");
        allOperations.add(")");
    }
    
    private void clearResults(){        
        //clear results
        allResults.clear();
        currentResult = -1;
    }
    
    private void clearInputs(){        
        //clear inputs
        operator = "";
        operationInputs.clear();
        allInputs.clear();
    }    

    private void clearOutputs(){        
        //clear outputs
        inputAreaOperations.setText("");
        resultAreaOperations.setText("");        
        inputAreaConversions.setText("");        
        resultAreaConversions.setText("");        
    }    

    private void clearParentheses(){        
        //clear number of parentheses
        numLeftParentheses=0;                
        numRightParentheses=0;
    }
        
    private void addListeners(){            
        parentheses1.addActionListener((ActionEvent f) -> {
            inputAreaOperations.append("(");
            allInputs.add("(");
            numLeftParentheses++;
        });

        parentheses2.addActionListener((ActionEvent f) -> {
            inputAreaOperations.append(")");
            allInputs.add(")");
            numRightParentheses++;
        });

        pow2.removeActionListener(pow2.getActionListeners()[0]);
        pow2.addActionListener((ActionEvent f) -> {
            inputAreaOperations.append("^2");
            operationInputs.add("^");
            allInputs.add("^");
            allInputs.add("2");
        });
        
        squaren.removeActionListener(squaren.getActionListeners()[0]);
        squaren.addActionListener((ActionEvent f) -> {
            inputAreaOperations.append("√");
            operationInputs.add("√");
            allInputs.add("√");
        });
        
        sqrt.removeActionListener(sqrt.getActionListeners()[0]);
        sqrt.addActionListener((ActionEvent f) -> {
            inputAreaOperations.append("√2");
            operationInputs.add("√");
            allInputs.add("√");
            allInputs.add("2");
        });
        
        pi.removeActionListener(pi.getActionListeners()[0]);
        pi.addActionListener((ActionEvent f) -> {            
            inputAreaOperations.append(Double.toString(Math.PI));
            allInputs.add("π");
        });

        euler.removeActionListener(euler.getActionListeners()[0]);
        euler.addActionListener((ActionEvent f) -> {         
            inputAreaOperations.append(Double.toString(Math.E));
            allInputs.add("e");
        });
                
        equals.addActionListener((ActionEvent f) -> {
            try {
                if(inputAreaOperations.getText().isEmpty()){
                    clearOutputs();
                }else{
                    String equationResult;
                    System.out.println("\n"+"[Starting new Equation]");
                    System.out.println("Operators: " + operationInputs);
                    System.out.println("Inputs: " + allInputs +"\n");
                    if(operator.equals(".") && operationInputs.isEmpty()){
                        equationResult = operations.fraction(Double.parseDouble(inputAreaOperations.getText()));
                        resultAreaOperations.setText("= "+equationResult);
                        allResults.add(equationResult);
                        currentResult++;
                        System.out.println("Result: " + equationResult);
                    }else{
                        equationResult = parseEquation();
                        if(!equationResult.equals("Syntax Error") && !equationResult.equals("Math Error")){
                            if(equationResult.equals("")){
                                resultAreaOperations.setText("Syntax Error");
                                inputAreaOperations.setText("");
                                clearInputs();
                                clearParentheses();                                                                
                            }else{
                                resultAreaOperations.setText("= "+equationResult);
                                allResults.add(equationResult);
                                currentResult++;
                            }
                        }else{
                            resultAreaOperations.setText(equationResult);
                        }
                    }
                }
            } catch (NumberFormatException e) {
                resultAreaOperations.setText(" Syntax Error ");
            }
        });
 
        point.removeActionListener(point.getActionListeners()[0]);
        point.addActionListener((ActionEvent f) -> {
            inputAreaOperations.append(".");
            if(operator.equals("")){
                operator = ".";                
            }
            allInputs.add(".");
        });
        clear.addActionListener((ActionEvent f) -> {
            clearOutputs();
            clearInputs();
            clearParentheses();
        });
        ans.addActionListener((ActionEvent f) -> {
            if(!allResults.isEmpty()){
                //get last result
                String lastResult = allResults.get(allResults.size()-1);
                //display it in inputArea
                inputAreaOperations.append(lastResult);
                //add it to inputs
                allInputs.add(lastResult);
            }
        });
        prevAns.addActionListener((ActionEvent f) -> {
            if(!allResults.isEmpty()){
                if((currentResult>-1) && (currentResult<allResults.size())){
                    //substract 1 to counter of current result
                    if(currentResult!=0){
                        currentResult--;
                    }
                    //get previous result
                    String prevResult = allResults.get(currentResult);
                    //display it in inputArea
                    inputAreaOperations.append(prevResult);
                    //add it to inputs
                    allInputs.add(prevResult);                   
                }
            }
        });        
        nextAns.addActionListener((ActionEvent f) -> {
            if(!allResults.isEmpty()){
                if((currentResult>-1) && (currentResult<allResults.size())){
                    //add 1 to counter of current result
                    if(currentResult!=(allResults.size()-1)){
                        currentResult++;
                    }
                    //get next result
                    String nextResult = allResults.get(currentResult);
                    //display it in inputArea
                    inputAreaOperations.append(nextResult);
                    //add it to inputs
                    allInputs.add(nextResult);                                       
                }
            }
        });            
        off.addActionListener((ActionEvent f) ->
            System.exit(0)
        );    
        convertButton.addActionListener((ActionEvent f) -> {
            if(!inputAreaConversions.getText().isEmpty() && !inputAreaConversions.getText().equals("")){
                int unit1 = unitOfMeasure1.getSelectedIndex();
                int unit2 = unitOfMeasure2.getSelectedIndex();
                int indexType = listOfConversions.getSelectedIndex();
                Double x1=1.0;
                Double x2=0.0;
                Double input = Double.parseDouble(inputAreaConversions.getText());                                    
                Double result;
                
                switch(indexType){                        
                            case 0:
                                x1 = volumeEquivalence[unit1];
                                x2 = volumeEquivalence[unit2];
                                break;
                            case 1:
                                x1 = longitudeEquivalence[unit1];
                                x2 = longitudeEquivalence[unit2];            
                                break;    
                            case 2:          
                                break;    
                            case 3:
                                x1 = weightEquivalence[unit1];
                                x2 = weightEquivalence[unit2];                                
                                break;    
                            case 4:    
                                x1 = pressureEquivalence[unit1];
                                x2 = pressureEquivalence[unit2];                                                                                        
                                break;    
                            default:
                                break;                                    
                }

                if(indexType==2){
                    result = operations.convertTemperature(unit1, unit2, input);
                }else{
                    result = input*x2/x1;
                }                
                resultAreaConversions.setText("= "+result.toString());                
            }
        });
        clearConvert.addActionListener((ActionEvent f) -> 
            clearOutputs()
        );
        tabsPane.addChangeListener((ChangeEvent e) -> {
            clearOutputs();
            clearInputs();
            clearParentheses();
            clearResults();
        });
    }
    
    public String parseEquation(){
        String equationResult;        
        //Check if there's any operation
        if(!operationInputs.isEmpty()){
            //Check if it doesn't have parentheses
            if(numLeftParentheses==0 && numRightParentheses==0){
                equationResult = solver.solveEquation(operationInputs,allInputs,allOperations,operations);
                System.out.println("Result: " + equationResult);        
            }
            //Check if it have parentheses
            else if((numLeftParentheses==numRightParentheses) && numLeftParentheses!=0 && numRightParentheses!=0){
                int posLParentheses=0; 
                int posRParentheses=0;                                    
                List<String> subEquation;
                List<String> subOperations;                                                      
                //While it has parentheses
                while(numLeftParentheses>0){
                    //subEquation = new ArrayList<>();
                    subOperations = new ArrayList<>();                    
                    //Get positions for the first parentheses
                    for (int i = 0; i < allInputs.size(); i++) {
                        if(allInputs.get(i).equals("(")){
                            posLParentheses = i;
                        }else if(allInputs.get(i).equals(")")){
                            posRParentheses = i;
                            break;
                        }
                    }                                        
                    //Get Equation inside parentheses                                         
                    subEquation = new ArrayList<>(allInputs.subList(posLParentheses+1, posRParentheses));                                        
                    System.out.println("Equation inside parenthesis: " + subEquation);                                        
                    //Get Operations of the Equation
                    for (int i = 0; i < subEquation.size(); i++) {                                        
                        if(allOperations.contains(subEquation.get(i))){
                            subOperations.add(subEquation.get(i));
                        }
                    }                                                               
                    //Solve Equation
                    equationResult = solver.solveEquation(subOperations,subEquation,allOperations,operations);                                        
                    //Put the result in the inputs                    
                    allInputs.set(posRParentheses,equationResult);                                                                                 
                    //Replace Equation with the result
                    for (int i = posLParentheses; i < posRParentheses; i++) {
                        allInputs.remove(posLParentheses);
                    }                    
                    System.out.println("After Solver: " + allInputs + "\n"); 
                    numLeftParentheses--; 
                }                                    
                System.out.println("Without Parentheses: " + allInputs);                                    
                if(allInputs.size()>1){                                        
                    subOperations = new ArrayList<>();                                        
                    //Get remaining operations
                    for (int i = 0; i < allInputs.size(); i++) {                                        
                        if(allOperations.contains(allInputs.get(i))){
                            subOperations.add(allInputs.get(i));
                        }
                    }                    
                    //Solve remaining equation      
                    equationResult = solver.solveEquation(subOperations,allInputs,allOperations,operations);
                }
                else{
                    //Get the input remaining
                    equationResult = allInputs.get(0);                                        
                }            
            }else{
                equationResult = "Syntax Error";                
            }                                                                  
        }else{
            if(allInputs.size()>1){
                equationResult = "";
            }else if(allInputs.size()==1){
                equationResult = allInputs.get(0);
            }else{
                equationResult = "";
            }
            clearInputs();
            clearParentheses();
        }                                                                
        return equationResult;
    }
}