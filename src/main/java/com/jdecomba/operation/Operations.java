package com.jdecomba.operation;

public class Operations {
	
    public double add(double n1 ,double n2){
	   return n1+n2;
    }
    
    public double subtract(double n1 ,double n2){
	   return n1-n2;
    }
    
    public double multiply(double n1 ,double n2){
	   return n1*n2;
    }
    
    public double divide(double n1 ,double n2){
           return n1/n2;
    }

    public double power(double n1 ,double n2){
	   return Math.pow(n1, n2);
    }
    
    public double square(double n1 ,double n2){           
	   return power(n1,(1.0/n2));
    }        	
    
    public double logarithm(double n1){           
	   return (Math.log(n1)/Math.log(10));
    }    
    
    public double percentage(double n1 ,double n2){           
           return (n1*n2)/100;
    }
    
    public String fraction(double n){           
        if (n < 0){
            return "-" + fraction(-n);
        }
        double tolerance = 1.0E-6;
        double h1=1; 
        double h2=0;
        double k1=0; 
        double k2=1;
        double b = n;
        do {
            double a = Math.floor(b);
            double aux = h1; h1 = a*h1+h2; h2 = aux;
            aux = k1; k1 = a*k1+k2; k2 = aux;
            b = 1/(b-a);
        } while (Math.abs(n-h1/k1) > n*tolerance);
        return h1+"/"+k1;
    }
    
    public Double convertTemperature(int unit1, int unit2, Double input){
        Double result = 0.0;
        switch(unit1){
            //Celcius
            case 0:
                switch (unit2) {
                    case 1:
                        result = (input*9/5) + 32;
                        break;
                    case 2:
                        result = input + 273.15;
                        break;
                    default:
                        result = input;
                        break;
                }
                break;
            //Fahrenheit
            case 1:
                switch (unit2) {
                    case 0:
                        result = (input-32)*5/9;
                        break;
                    case 2:
                        result = ((input-32)*5/9) + 273.15;
                        break;
                    default:
                        result = input;
                        break;
                }
                break;
            //Kelvin
            case 2:
                switch (unit2) {
                    case 0:
                        result = input - 273.15;
                        break;
                    case 1:
                        result = ((input - 273.15)*9/5) + 32;
                        break;
                    default:
                        result = input;
                        break;
                }
                break;
            default:
                break;
        }                
        return result;
    }
}